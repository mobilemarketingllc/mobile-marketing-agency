<?php
//session_start();
// Defines
define( 'FL_CHILD_THEME_DIR', get_stylesheet_directory() );
define( 'FL_CHILD_THEME_URL', get_stylesheet_directory_uri() );
// Classes
require_once 'classes/class-fl-child-theme.php';
// Actions
add_action( 'wp_enqueue_scripts', 'FLChildTheme::enqueue_scripts', 1000 );
add_action( 'wp_enqueue_scripts', function(){
    //wp_enqueue_script("child-script",get_stylesheet_directory_uri()."/script.js",array("jquery"));
});
// Move Yoast to bottom
function yoasttobottom()
{
    return 'low';
}
add_filter('wpseo_metabox_prio', 'yoasttobottom');