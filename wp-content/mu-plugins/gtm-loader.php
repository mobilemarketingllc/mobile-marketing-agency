<?php
/*
Plugin Name: MM Google Tag Manager
Plugin URI:  http://mobile-marketing.agency
Description: For managing your sites Google Tag Manager script insertion.
Version:     0.1.5
Author:      mobile-marketing.agency
Author URI:  http://mobile-marketing.agency
*/

$required_php_version = '5.3.0';
if (version_compare(phpversion(), $required_php_version, '>')) {
	require_once __DIR__ . '/gtm-manager/gtm-manager.php';
}
